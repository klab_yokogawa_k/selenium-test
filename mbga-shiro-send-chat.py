# coding: UTF-8
# vi: set ts=2 sw=2 sts=0 et:

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
import time

chrome_path = '/home/yokogawa-k/local/selenium/chromedriver'
# set options
# http://stackoverflow.com/questions/15165593/set-chrome-prefs-with-python-binding-for-selenium-in-chromedriver
cap = {
        "browserName": "chrome",
        "platform": "ANY",
        "javascriptEnabled": True,
        "proxy": {
            "httpProxy": None,
            "ftpProxy": None,
            "noProxy": None,
            "proxyType": "DIRECT",
            "class": "org.openqa.selenium.Proxy",
            "autodetect": False,
            },
        }
opt = webdriver.ChromeOptions()
opt.add_argument("window-size=500,800")
opt.add_argument("allow-running-insecure-content")
opt.add_argument("disable-web-security")
opt.add_argument("disk-cache-dir=/home/yokogawa-k/local/var/tmp/cache/")
opt.add_argument("no-referrers")
opt.add_argument("'chrome.prefs': {'profile.managed_default_content_settings.images' : 2}")
opt.add_argument('--user-agent="Mozilla/5.0 (Linux; U; Android 1.6; en-us; HTC Magic Build/DRD20) AppleWebKit/528.5+ (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1"')
opt.add_argument('--user-data-dir=/home/yokogawa-k/settings/chrome/selenium')

driver = webdriver.Chrome(executable_path=chrome_path, chrome_options=opt, desired_capabilities=cap)

driver.get("http://sp.pf.mbga.jp/12000022")

WebDriverWait(driver, 10).until(lambda driver : driver.title.startswith(u"しろつく"))
# Top -> bounus
# linkじゃだめ？
#link = driver.find_elements_by_css_selector('#container > div:nth-child(6) > a:nth-child(1)')
#link = driver.find_elements_by_xpath('//*[@id="container"]/div[4]/a[1]')[0]
# banner を検索
driver.find_elements_by_xpath('//*[@id="container"]/div[4]/a[1]/div/img')[0].click()
# チャットへ移動
driver.find_elements_by_xpath('//*[@id="container"]/div[3]/a[2]')[0].click()
# 一番上の人にチャットを送信
driver.find_elements_by_xpath('//*[@id="container"]/div[3]/div/div[4]/form[1]/table/tbody/tr/td[2]/input[5]')[0].submit()

try:
  msg = driver.find_elements_by_xpath('//*[@id="sp-global-container"]/ul/li/a/div/span')[0]
  if msg.text == u"しろつくへ戻る":
    msg.click()
except:
  driver.find_elements_by_xpath('//*[@id="mm-send-form"]/div[2]/div[4]/input')[0].submit()
  driver.find_elements_by_xpath('//*[@id="mm-send-form"]/div[2]/div/input')[0].submit()
  driver.find_elements_by_xpath('//*[@id="sp-global-container"]/div[6]/ul/li[2]/a/div/span')[0].click()

back = driver.find_element_by_xpath('//*[@id="container"]/div[2]/div[2]/a[1]')
if back.text == u"更にﾁｬｯﾄを送る":
  back.click()
# inputElement = driver.find_element_by_name("q")
# inputElement.send_keys("Cheese!!")

# inputElement.submit()

print driver.title

driver.quit()

#try:
#    WebDriverWait(driver, 10).until(lambda driver : driver.title.lower().startswith("cheese!"))
#    print driver.title
#finally:
#    driver.quit()

