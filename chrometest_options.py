# coding: UTF-8

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
import time

chrome_path = '/home/yokogawa-k/local/selenium/chromedriver'
# set options
# http://stackoverflow.com/questions/15165593/set-chrome-prefs-with-python-binding-for-selenium-in-chromedriver
cap = {
        "browserName": "chrome",
        "platform": "ANY",
        "javascriptEnabled": True,
        "proxy": {
            "httpProxy": None,
            "ftpProxy": None,
            "noProxy": None,
            "proxyType": "DIRECT",
            "class": "org.openqa.selenium.Proxy",
            "autodetect": False,
            },
        }
opt = webdriver.ChromeOptions()
opt.add_argument("window-size=600,300")
opt.add_argument("allow-running-insecure-content")
opt.add_argument("disable-web-security")
opt.add_argument("disk-cache-dir=/home/yokogawa-k/local/var/tmp/cache/")
opt.add_argument("no-referrers")
opt.add_argument("'chrome.prefs': {'profile.managed_default_content_settings.images' : 2}")

driver = webdriver.Chrome(executable_path=chrome_path, chrome_options=opt, desired_capabilities=cap)

driver.get("http://www.google.com")

inputElement = driver.find_element_by_name("q")
inputElement.send_keys("Cheese!!")

inputElement.submit()

print driver.title

try:
    WebDriverWait(driver, 10).until(lambda driver : driver.title.lower().startswith("cheese!"))
    print driver.title
finally:
    driver.quit()

