# coding: UTF-8

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
import time

driver = webdriver.Chrome( '/home/yokogawa-k/local/selenium/chromedriver')

driver.get("http://www.google.com")

inputElement = driver.find_element_by_name("q")
inputElement.send_keys("Cheese!!")

inputElement.submit()

print driver.title

try:
    WebDriverWait(driver, 10).until(lambda driver : driver.title.lower().startswith("cheese!"))
    print driver.title
finally:
    driver.quit()

